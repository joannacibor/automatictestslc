package resources;

/**
 * Decided to create separate file for all asserted texts in case if someday website became i18n
 */
public class StringsPl {
    // Log in page
    static final public String incorrectPasswordAlert = "Nieprawidłowe hasło.";

    // Search page
    static final public String noResultsInSearch = "Przykro nam, niczego nie znaleźliśmy.";
    static final public String noResultsInSearchAddNewBook = "Nie znalazłeś książki? Dodaj ją samodzielnie do bazy";
    static final public String tooShortSearchPhrase = "Wyszukiwane hasło jest za krótkie. Minimalna długość frazy to dwa znaki. Jeśli szukasz tytułu książki, spróbuj wyszukiwania po autorze.";
}
