package resources.dataProviders;

import org.testng.annotations.DataProvider;

public class SearchDataProvider {

    @DataProvider(name = "searchDataProvider")
    public static Object[][] dataProviderfunc() {
        return new Object[][]{{"milczenie owiec"}, {"Dostojewski"}, {"buszujacy"}, {"j.k. rowling"}};
    }
}