package tests.logIn;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.common.TopMenuPage;
import pageObjects.utlis.LogInUtil;

import static base.Config.*;

public class LogOutTest extends TestBase {
    TopMenuPage topMenuPage;

    @BeforeMethod
    protected void beforeClass() {
        topMenuPage = new TopMenuPage(driver);

        driver.get(BASE_URL);
    }

    @Test
    public void logInAndThenLogOut() {
        LogInUtil.loginFastForward(driver, realUserEmail, realUserPass);
        topMenuPage
                .clickUserDropdown()
                .clickSignOut();

        Assert.assertTrue(topMenuPage.isLoginButtonVisible());
    }
}