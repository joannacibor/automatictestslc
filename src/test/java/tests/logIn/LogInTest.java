package tests.logIn;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.common.TopMenuPage;
import pageObjects.menuPages.LogInPage;

import static base.Config.*;
import static resources.StringsPl.incorrectPasswordAlert;

public class LogInTest extends TestBase {
    TopMenuPage topMenuPage;
    LogInPage logInPage;

    @BeforeMethod
    protected void beforeMethod() {
        topMenuPage = new TopMenuPage(driver);
        logInPage = new LogInPage(driver);

        driver.get(BASE_URL);
    }

    @Test
    public void logInWithCorrectData() {
        topMenuPage.clickLogInButton();

        logInPage
                .enterEmail(realUserEmail)
                .enterPass(realUserPass)
                .clickLogIn();

        Assert.assertEquals(realUserUsername, topMenuPage.getUsernameFromDropdown());
    }

    @Test
    protected void logInWithInCorrectData() {
        topMenuPage.clickLogInButton();

        logInPage
                .enterEmail("aaa@aaa.com")
                .enterPass("admin1")
                .clickLogIn();

        Assert.assertTrue(logInPage.isAlertVisible());
        Assert.assertTrue(logInPage.isAlertTextStartsWithExpected(incorrectPasswordAlert));
    }
}