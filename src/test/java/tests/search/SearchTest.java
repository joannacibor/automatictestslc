package tests.search;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.common.TopMenuPage;
import pageObjects.search.SearchPage;
import pageObjects.search.SearchResultObject;
import pageObjects.search.SearchResultPage;
import resources.dataProviders.SearchDataProvider;

import static base.Config.BASE_URL;
import static resources.StringsPl.*;

public class SearchTest extends TestBase {
    TopMenuPage topMenuPage;
    SearchPage searchPage;
    SearchResultPage searchResultPage;

    @BeforeMethod
    protected void beforeMethod() {
        topMenuPage = new TopMenuPage(driver);
        searchPage = new SearchPage(driver);
        searchResultPage = new SearchResultPage(driver);

        driver.get(BASE_URL);
    }

    @Test(dataProvider = "searchDataProvider", dataProviderClass = SearchDataProvider.class)
    public void searchDifferentPhrasesAndCheckResultsAreReturned(String searchPhrase) {
        topMenuPage
                .enterTextToSearchTextBox(searchPhrase)
                .clickSearchButton();

        Assert.assertTrue(searchResultPage.getNumberOfSearchResults() > 0);

        for (SearchResultObject result : searchResultPage.getListOfSearch()) {
            Assert.assertNotNull(result.getImage());
            Assert.assertNotNull(result.getTitle());
            Assert.assertNotNull(result.getAuthor());
            Assert.assertTrue(result.getRate() >= 0);
            Assert.assertTrue(result.getNumberOfNotes() >= 0);
            Assert.assertTrue(result.getNumberOfOpinions() >= 0);
            Assert.assertTrue(result.getNumberOfReaders() >= 0);
        }
    }

    @Test
    public void invalidTextSearch() {
        topMenuPage
                .enterTextToSearchTextBox("bbbaaaaazzzzzzxxxxxx")
                .clickSearchButton();

        Assert.assertEquals(searchPage.getTextOfNothingFoundLabel(), noResultsInSearch);
        Assert.assertEquals(searchPage.getTextOfAddBookLabel(), noResultsInSearchAddNewBook);
        searchPage.clickAddBook();
    }

    @Test
    public void tooShortSearch() {
        topMenuPage
                .enterTextToSearchTextBox("a")
                .clickSearchButton();

        Assert.assertEquals(searchPage.getTextOfTooShortPhraseLabel(), tooShortSearchPhrase);
    }
}
