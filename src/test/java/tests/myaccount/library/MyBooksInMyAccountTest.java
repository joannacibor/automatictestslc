package tests.myaccount.library;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.common.TopMenuPage;
import pageObjects.menuPages.MyAccountPage;
import pageObjects.utlis.LogInUtil;

import static base.Config.*;

public class MyBooksInMyAccountTest extends TestBase {
    TopMenuPage topMenuPage;
    MyAccountPage myAccountPage;

    @BeforeMethod
    protected void beforeMethod() {
        topMenuPage = new TopMenuPage(driver);
        myAccountPage = new MyAccountPage(driver);
        driver.get(BASE_URL);
    }

    @Test
    protected void booksAppearWhenUserHasBooks() {
        LogInUtil.loginFastForward(driver, realUserEmail, realUserPass);

        topMenuPage
                .clickUserDropdown()
                .clickMyAccount();

        Assert.assertTrue(myAccountPage.getNumberOfReadBooks() > 0);
        Assert.assertTrue(myAccountPage.getNumberOfNowReadBooks() > 0);
        Assert.assertTrue(myAccountPage.getNumberOfWantToReadBooks() > 0);
    }

    @Test
    protected void noBookAppearsWhenUserHasNoBooks() {
        LogInUtil.loginFastForward(driver, realUserEmailNoBooks, realUserPassNoBooks);

        topMenuPage
                .clickUserDropdown()
                .clickMyAccount();

        Assert.assertEquals(myAccountPage.getNumberOfReadBooks(), 0);
        Assert.assertEquals(myAccountPage.getNumberOfNowReadBooks(), 0);
        Assert.assertEquals(myAccountPage.getNumberOfWantToReadBooks(), 0);
    }
}
