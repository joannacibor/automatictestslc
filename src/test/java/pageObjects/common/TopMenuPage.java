package pageObjects.common;

import base.PageObjectBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Top menu with logo, log in, sign in and search functions
 */
public class TopMenuPage extends PageObjectBase {
    public TopMenuPage(WebDriver driver) {
        super(driver);
    }

    // region findby
    @FindBy(css = "header a[href$='/zaloguj']")
    WebElement loginButton;

    @FindBy(css = "header span[data-toggle='dropdown']")
    WebElement userDropdown;

    @FindBy(css = "header a.dropdown-item:first-child")
    WebElement myAccountOption;

    @FindBy(css = "header a[class^='dropdown'][href$='wyloguj']")
    WebElement signOutOption;

    @FindBy(id = "topSearchPhrase")
    WebElement searchTextBox;

    @FindBy(id = "topSearchButton")
    WebElement searchButton;
    // endregion

    public TopMenuPage clickLogInButton() {
        waitForAndClick(loginButton);
        return this;
    }

    public boolean isLoginButtonVisible() {
        return waitForVisibility(loginButton);
    }

    public String getUsernameFromDropdown() {
        return waitForAndGetText(userDropdown);
    }

    public TopMenuPage clickUserDropdown() {
        waitForAndClick(userDropdown);
        return this;
    }

    public TopMenuPage clickMyAccount() {
        waitForAndClick(myAccountOption);
        return this;
    }

    public TopMenuPage clickSignOut() {
        waitForAndClick(signOutOption);
        return this;
    }

    public TopMenuPage enterTextToSearchTextBox(String text) {
        waitForAndSendKeys(searchTextBox, text);
        return this;
    }

    public TopMenuPage clickSearchButton() {
        waitForAndClick(searchButton);
        return this;
    }
}

