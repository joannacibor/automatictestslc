package pageObjects.common;

import base.PageObjectBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TermsOfUsePopupPage extends PageObjectBase {
    public TermsOfUsePopupPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "div[class='modal fade show'] a[onclick='toWww();']")
    WebElement acceptTermsOfUseButton;

    public void clickAcceptTermsOfUseButton() {
        waitForAndClick(acceptTermsOfUseButton);
    }
}
