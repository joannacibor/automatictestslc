package pageObjects.utlis;

import org.openqa.selenium.WebDriver;
import pageObjects.menuPages.LogInPage;

import static base.Config.BASE_URL;
import static base.Config.LOGIN_URL;

public class LogInUtil {

    public static void loginFastForward(WebDriver driver, String user, String pass) {
        driver.get(BASE_URL + LOGIN_URL);

        new LogInPage(driver)
                .enterEmail(user)
                .enterPass(pass)
                .clickLogIn();
    }
}
