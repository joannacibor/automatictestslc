package pageObjects.utlis;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static base.Config.SCREENSHOT_PATH;

public class ScreenshotCapture {

    public static void takeScreenshot(WebDriver driver, String testCaseName) throws IOException {
        // Screenshot time stamp
        SimpleDateFormat timeFormater = new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
        Date date = new Date();
        String screenTime = timeFormater.format(date);

        //Take and save screenshot
        TakesScreenshot scrShot = ((TakesScreenshot) driver);
        File srcFile = scrShot.getScreenshotAs(OutputType.FILE);
        File destFile = new File(SCREENSHOT_PATH + screenTime + " " + testCaseName + ".jpg");
        FileUtils.copyFile(srcFile, destFile);
    }
}