package pageObjects.search;

public class SearchResultObject {
    private String image;
    private String title;
    private String author;
    private double rate;
    private int numberOfNotes;
    private int numberOfReaders;
    private int numberOfOpinions;

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public double getRate() {
        return rate;
    }

    public int getNumberOfNotes() {
        return numberOfNotes;
    }

    public int getNumberOfReaders() {
        return numberOfReaders;
    }

    public int getNumberOfOpinions() {
        return numberOfOpinions;
    }

    public static final class Builder {
        private String image;
        private String title;
        private String author;
        private double rate;
        private int numberOfNotes;
        private int numberOfReaders;
        private int numberOfOpinions;

        public Builder image(String image) {
            this.image = image;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder rate(double rate) {
            this.rate = rate;
            return this;
        }

        public Builder numberOfNotes(int numberOfNotes) {
            this.numberOfNotes = numberOfNotes;
            return this;
        }

        public Builder numberOfReaders(int numberOfReaders) {
            this.numberOfReaders = numberOfReaders;
            return this;
        }

        public Builder numberOfOpinions(int numberOfOpinions) {
            this.numberOfOpinions = numberOfOpinions;
            return this;
        }

        public SearchResultObject build() {
            SearchResultObject searchResultObject = new SearchResultObject();
            searchResultObject.image = this.image;
            searchResultObject.title = this.title;
            searchResultObject.author = this.author;
            searchResultObject.rate = this.rate;
            searchResultObject.numberOfNotes = this.numberOfNotes;
            searchResultObject.numberOfReaders = this.numberOfReaders;
            searchResultObject.numberOfOpinions = this.numberOfOpinions;
            return searchResultObject;
        }
    }

    public static Builder builder() {
        return new Builder();
    }
}