package pageObjects.search;

import base.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class SearchResultPage extends PageObjectBase {
    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    //region WebElements and deeper selectors
    @FindBy(css = ".authorAllBooks__single")
    private List<WebElement> listOfResults;

    private String getImage(WebElement element) {
        return element.findElement(By.cssSelector(".authorAllBooks__single img[src$='jpg']")).getAttribute("src");
    }

    private String getTitle(WebElement element) {
        return element.findElement(By.cssSelector("a[class^='authorAllBooks__singleTextTitle']")).getText();
    }

    private String getAuthor(WebElement element) {
        return element.findElement(By.cssSelector("div[class^='authorAllBooks__singleTextAuthor']")).getText();
    }

    private double getRating(WebElement element) {
        return convertStringToDouble(element.findElement(By.cssSelector(".listLibrary__ratingStarsNumber")));
    }

    private int getNumberOfNotes(WebElement element) {
        return (convertStringToInt(element.findElement(By.cssSelector(".listLibrary__ratingAll"))));
    }

    private int getNumberOfReaders(WebElement element) {
        return (convertStringToInt(element.findElement(By.cssSelector("span[class^='small grey mr-2']"))));
    }

    private int getNumberOfROpinions(WebElement element) {
        return (convertStringToInt(element.findElement(By.cssSelector("span[class^='ml-2 small grey']"))));
    }
    //endregion

    public int getNumberOfSearchResults() {
        return listOfResults.size();
    }

    public List<SearchResultObject> getListOfSearch() {
        List<SearchResultObject> listOfSearchObjects = new ArrayList<SearchResultObject>();

        for (WebElement result : listOfResults) {
            SearchResultObject searchResultObject = SearchResultObject.builder()
                    .image(getImage(result))
                    .title(getTitle(result))
                    .author(getAuthor(result))
                    .rate(getRating(result))
                    .numberOfNotes(getNumberOfNotes(result))
                    .numberOfReaders(getNumberOfReaders(result))
                    .numberOfOpinions(getNumberOfROpinions(result))
                    .build();
            listOfSearchObjects.add(searchResultObject);
        }
        return listOfSearchObjects;
    }

    private int convertStringToInt(WebElement element) {
        String text = element.getText().replaceAll("\\D+", "");
        return Integer.parseInt(text);
    }

    private double convertStringToDouble(WebElement element) {
        String text = element.getText().replace(",", ".");
        text = text.replace("/[^0-9.]/g", "");
        return Double.parseDouble(text);
    }
}