package pageObjects.search;

import base.PageObjectBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage extends PageObjectBase {
    public SearchPage(WebDriver driver) {
        super(driver);
    }

    // region WebElements
    @FindBy(className = "search__no-results")
    private WebElement tooShortPhraseLabel;

    @FindBy(css = "#search")
    private WebElement nothingFoundLabel;

    @FindBy(className = "t-600")
    private WebElement addBookLabel;

    @FindBy(css = "#search a[href='/ksiazka/dodaj']")
    private WebElement addBookButton;
    // endregion

    public String getTextOfTooShortPhraseLabel() {
        return waitForAndGetText(tooShortPhraseLabel);
    }

    public String getTextOfNothingFoundLabel() {
        // naughty solution to get only 1st String without container
        return waitForAndGetText(nothingFoundLabel).substring(0, waitForAndGetText(nothingFoundLabel).indexOf('\n'));
    }

    public String getTextOfAddBookLabel() {
        return waitForAndGetText(addBookLabel);
    }

    public SearchPage clickAddBook() {
        waitForAndClick(addBookButton);
        return this;
    }
}