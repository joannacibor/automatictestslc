package pageObjects.menuPages;

import base.PageObjectBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MyAccountPage extends PageObjectBase {
    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "div.dashBoardAccount__libraryItemBooks")
    List<WebElement> categoriesOfLibrary;

    public int getNumberOfReadBooks() {
        return getNumberOfBooksFromCategory(0);
    }

    public int getNumberOfNowReadBooks() {
        return getNumberOfBooksFromCategory(1);
    }

    public int getNumberOfWantToReadBooks() {
        return getNumberOfBooksFromCategory(2);
    }

    private int getNumberOfBooksFromCategory(int category) {
        String toNumber = categoriesOfLibrary.get(category).getText().replaceAll("[^0-9]", "");
        return Integer.parseInt(toNumber);
    }
}
