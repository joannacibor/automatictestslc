package pageObjects.menuPages;

import base.PageObjectBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static resources.StringsPl.incorrectPasswordAlert;

public class LogInPage extends PageObjectBase {
    public LogInPage(WebDriver driver) {
        super(driver);
    }

    // region findby
    @FindBy(id = "Email")
    WebElement loginTextBox;

    @FindBy(id = "Password")
    WebElement passwordTextBox;

    @FindBy(css = "form[class=\"form needs-validation\"] button")
    WebElement loginButton;

    @FindBy(css = "div[class*=\"alert-danger\"]")
    WebElement invalidPasswordAlert;
    // endregion

    public LogInPage enterEmail(String email) {
        waitForAndSendKeys(loginTextBox, email);
        return this;
    }

    public LogInPage enterPass(String pass) {
        waitForAndSendKeys(passwordTextBox, pass);
        return this;
    }

    public void clickLogIn() {
        waitForAndClick(loginButton);
    }

    public boolean isAlertTextStartsWithExpected(String text) {
        return incorrectPasswordAlert.startsWith(text);
    }

    public boolean isAlertVisible() {
        return waitForVisibility(invalidPasswordAlert);
    }
}