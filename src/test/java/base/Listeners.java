package base;

import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;
import pageObjects.utlis.ScreenshotCapture;

import java.io.IOException;

public class Listeners implements ITestListener {
    public void onTestSuccess(ITestResult result) {
        System.out.println("TC Pass: " + result.getName());
    }

    public void onTestFailure(ITestResult result) {
        System.out.println("TC Fail: " + result.getName());

        // How to avoid driver-static and pass driver to listener
        // https://stackoverflow.com/questions/17605783/non-static-driver-and-screenshot-listener-in-testng
        // I cast test class Object (that compiler wouldn't know what it is) to TestBase class, its parent, so I can user getDriver method

        TestBase testClass = (TestBase) result.getInstance();
        WebDriver driver = testClass.getDriver();

        if (driver != null) {
            try {
                ScreenshotCapture.takeScreenshot(driver, result.getName());
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Error occurred, fail test case screenshot not executed: " + result.getName());
            }
        } else {
            System.out.println("Driver not found, fail test case screenshot not executed: " + result.getName());
        }
    }
}