package base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pageObjects.common.TermsOfUsePopupPage;

import static base.Config.BASE_URL;

public class TestBase {

    protected WebDriver driver;
    TermsOfUsePopupPage termsOfUsePopupPage;

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        closeTermsOfUsePopup();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

    /**
     * closes terms of use pop-up that appears after 1st site open
     */
    public void closeTermsOfUsePopup() {
        termsOfUsePopupPage = new TermsOfUsePopupPage(driver);
        driver.get(BASE_URL);
        termsOfUsePopupPage.waitForUrl(BASE_URL);
        termsOfUsePopupPage.clickAcceptTermsOfUseButton();
    }
}