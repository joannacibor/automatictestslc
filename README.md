# README #

 

### What is this repository for? ###

* This is training project for GUI test automation for known polish books database portal - www.lubimyczytac.pl. 
* I decided to choose a commercial website to face real problems that usually cannot be met on test websites - tricky wait implementation, challenging WebElement locators, cases requiring JavaScriptExecutor etc.
* I focused to develop complex project with usage of most popular elements (DataProvider, Listeners, Screenshots execution etc.) 
* I didn't implement scenarios that seems to be the most important (e.g. test of registration process) if they affect website negatively - there is no registration, order, opinions add etc. 

### How do I get set up? ###

* Install Chrome
* Clone repository
* Run tests from testng_allTests.xml file
* That's all - have fun!

### Who do I talk to? ###

* https://www.linkedin.com/in/j-cibor/